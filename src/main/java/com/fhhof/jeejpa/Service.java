/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa;

import com.fhhof.jeejpa.jpa.Tag;
import com.fhhof.jeejpa.jpa.SimpleEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author kaiseran
 */
@Stateless
public class Service {
    
    
    @PersistenceContext
    EntityManager em;
    
    public SimpleEntity persistSimpleEntity()  {
        SimpleEntity entity = new SimpleEntity();
        Tag t = new Tag();
        Tag t2 = new Tag();
        entity.addTag(t);
        entity.addTag(t2);
        em.persist(entity);
        
        System.out.println("Hallo Welt");
        em.flush();
        return entity;
    }
    
    public List<SimpleEntity> getAll() {
        TypedQuery<SimpleEntity> query = em.createQuery("SELECT s FROM SimpleEntity s JOIN FETCH s.myTags ", SimpleEntity.class);
        List<SimpleEntity> resultList = query.getResultList();
        return resultList;
        
    }
}
