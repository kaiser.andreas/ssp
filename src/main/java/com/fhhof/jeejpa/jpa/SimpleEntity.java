/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class SimpleEntity implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
   
    @OneToMany(mappedBy = "simpleEntity", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private List<Tag> myTags = new ArrayList<>();
    
    @OneToOne
    SimpleEntity parent;

    

    public SimpleEntity() {
    }
    
    public boolean addTag(Tag t) {
        t.setSimpleEntity(this);
        return this.myTags.add(t);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public List<Tag> getMyTags() {
        return myTags;
    }

    public void setMyTags(List<Tag> myTags) {
        this.myTags = myTags;
    }

}
