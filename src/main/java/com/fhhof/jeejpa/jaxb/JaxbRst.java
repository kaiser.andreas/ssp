package com.fhhof.jeejpa.jaxb;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("jaxb")
public class JaxbRst {

    @GET
    @Produces(value = {MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Student generateStudent() {
        LocalDate birth = LocalDateTime.of(1984, Month.MARCH, 5, 0, 0).toLocalDate();
        Date birthday = Date.from(birth.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Student student = new Student("max", "mustermann", birthday);

        return student;
    }
    @GET
    @Path("lessons")
    public Student generateStudentWithLessons() {
        Student s = this.generateStudent();
        
        List<Lessons> lessonlList =  Arrays.asList(new Lessons("SSP", 17), new Lessons("PPS", 1));
        s.setLessonsList(lessonlList);

        return s;
    }
    
    @POST
    @Consumes(value = {MediaType.APPLICATION_JSON})
    public String createStudent(Student student) {
        return student.toString();
    }
    
    
    

}
