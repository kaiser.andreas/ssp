/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jaxb;

import java.io.Serializable;
import javax.json.bind.annotation.JsonbProperty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kaiseran
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Lessons implements Serializable {

    private String name;
    @XmlElement(name = "credits")
    @JsonbProperty(value = "credits")
    private int ects;

    public Lessons() {
    }

    public Lessons(String name, int ects) {
        this.name = name;
        this.ects = ects;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getEcts() {
        return ects;
    }

    public void setEcts(int ects) {
        this.ects = ects;
    }

    @Override
    public String toString() {
        return "Lessons{" + "name=" + name + ", ects=" + ects + '}';
    }
    
    

}
