/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jaxrs;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import javax.validation.constraints.NotNull;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

@Path("simple")
public class MiniRest {

    @GET
    public String sayHello() {
        return "Hello World!";
    }

    @GET
    @Path("item/{order}")
    public String getItemByOrder(@PathParam("order") String orderId) {
        return "You item was " + orderId;
    }

    @GET
    @Path("demo/{code: [0-9]{6}}")
    public String getCode(@PathParam("code") String code) {
        return "You code was " + code;
    }

    @GET
    @Path("user")
    public String getUser(@QueryParam("id") String id, @QueryParam("status") boolean status) {
        return "Here is user with id " + id + " Status " + status;
    }

    @GET
    @Path("customer/order")
    public String getOrders(@MatrixParam("flag") String flag) {
        return "Here is Order flagged with " + flag;
    }

    @GET
    @Path("cookie")
    public Response getCookie(@CookieParam("knownAs") String nickname) {
        Optional<String> nick = Optional.ofNullable(nickname);
        if (nick.isPresent()) {
            return Response.ok("Hallo Bekannter " + nick.get()).build();
        }
        Random r = new Random();
        int id = r.nextInt(1000);
        NewCookie cookie = new NewCookie("knownAs", "User_" + id);
        return Response.ok("Hallo Fremder").cookie(cookie).build();
    }

}
