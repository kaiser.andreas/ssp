/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jaxrs.config;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value = "api")
public class JAXRSApplication extends Application{

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(com.fhhof.jeejpa.jaxrs.Demo.class);
        resources.add(com.fhhof.jeejpa.jaxrs.Primer.class);
        resources.add(com.fhhof.jeejpa.jaxrs.MiniRest.class);
        resources.add(com.fhhof.jeejpa.jaxb.JaxbRst.class);
        return resources;
    }
    
}
