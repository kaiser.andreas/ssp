/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jaxrs;

import java.util.stream.IntStream;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author kaiseran
 */
@Path("prime")
public class Primer {

    @GET
    @Path("{value}")
    public Response isPrime(@PathParam("value") int value) {

        boolean result = value > 2
                && IntStream.rangeClosed(2, (int) Math.sqrt(value))
                        .noneMatch(n -> (value % n == 0));

        return Response.ok(result).build();

    }
    
    @GET
    @Path("notPrime/{value}")
    public Response isNotPrime(@PathParam("value") int value) {
    	boolean result = value > 2
                && IntStream.rangeClosed(2, (int) Math.sqrt(value))
                        .noneMatch(n -> (value % n == 0));

        return Response.ok(!result).build();
	}

}
