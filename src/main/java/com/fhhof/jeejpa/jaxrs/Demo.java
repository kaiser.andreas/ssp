/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jaxrs;

import com.fhhof.jeejpa.Service;
import com.fhhof.jeejpa.jpa.SimpleEntity;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author kaiseran
 */
@Path("demo")
@ApplicationScoped
public class Demo {
    
   @Inject
   Service service;
    
    @GET
    public SimpleEntity persistSimpleEntity()  {
        return service.persistSimpleEntity();
    }
    
    @GET()
    @Path("all")
    public List<SimpleEntity> getAll() {
       return service.getAll();
        
    }
    
}
