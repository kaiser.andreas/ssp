/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jaxrs;

import com.github.hanleyt.JerseyExtension;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.server.ResourceConfig;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class PrimerTest {

    public PrimerTest() {
    }

    @RegisterExtension
    JerseyExtension jerseyExtension = new JerseyExtension(this::configureJersey);

    private Application configureJersey(ExtensionContext extensionContext) {
        return new ResourceConfig(Primer.class);
    }

   
    
    

    @ParameterizedTest()
    @ValueSource(ints = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103})
    public void testIsPrime(Integer value, WebTarget target) {
        
        Boolean result = target.path("prime/" + value).request().get(Boolean.class);
        assertTrue(result);
    }

    @ParameterizedTest
    @ValueSource(ints = {4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30, 32, 33, 34, 35, 36, 38, 39, 40, 42, 44, 45, 46})
    public void testIsNontPrime(int value, WebTarget target) {
        Boolean result = target.path("prime/" + value).request().get(Boolean.class);
        assertFalse(result);
    }

}
