/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhhof.jeejpa.jaxb;

import com.github.hanleyt.JerseyExtension;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.ResourceConfig;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.RegisterExtension;

public class JaxbRstTest {

    public JaxbRstTest() {
    }

    @RegisterExtension
    JerseyExtension jerseyExtension = new JerseyExtension(this::configureJersey);

    private Application configureJersey(ExtensionContext extensionContext) {
        return new ResourceConfig(JaxbRst.class);
    }

    @Test
    public void testGetXML(WebTarget target) {

        Response resp = target.path("jaxb/").request(MediaType.APPLICATION_XML).get();
        if (resp.bufferEntity() && resp.getEntity() instanceof ByteArrayInputStream) {
            ByteArrayInputStream bais = (ByteArrayInputStream) resp.getEntity();
            int n = bais.available();
            byte[] bytes = new byte[n];
            bais.read(bytes, 0, n);
            String s = new String(bytes, StandardCharsets.UTF_8);
            System.out.println(s);
            assertTrue(s.contains("<student><firstname>max</firstname><lastname>mustermann</lastname>"));
        } else {
            fail("Not a valid xml");
        }
    }

    @Test
    public void testGetXMLToObject(WebTarget target) {

        Student result = target.path("jaxb/").request(MediaType.APPLICATION_XML).get(Student.class);
        assertNotNull(result);
    }

    @Test
    public void testGetJSON(WebTarget target) {

        Response resp = target.path("jaxb/").request(MediaType.APPLICATION_JSON).get();
        if (resp.bufferEntity() && resp.getEntity() instanceof ByteArrayInputStream) {
            ByteArrayInputStream bais = (ByteArrayInputStream) resp.getEntity();
            int n = bais.available();
            byte[] bytes = new byte[n];
            bais.read(bytes, 0, n);
            String s = new String(bytes, StandardCharsets.UTF_8);
            System.out.println(s);
            assertTrue(s.contains("firstname\":\"max\",\"lastname\":\"mustermann\""));
        } else {
            fail("Not a valid json");
        }
    }

    @Test
    public void testGetJSONToObject(WebTarget target) {

        Student result = target.path("jaxb/").request(MediaType.APPLICATION_JSON).get(Student.class);
        assertNotNull(result);
    }

    @Test
    public void testPOSTJSON(WebTarget target) {
        LocalDate birth = LocalDateTime.of(1984, Month.MARCH, 5, 0, 0).toLocalDate();
        Date birthday = Date.from(birth.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Student s = new Student("test", "tester", birthday);
        Entity<Student> entity = Entity.entity(s, MediaType.APPLICATION_JSON);
        Response result = target.path("jaxb/").request().post(entity);
        assertNotNull(result);
        
        assertEquals(200, result.getStatus());
    }

}
